
public abstract class UWECPerson {
	private int uwecId;
	private String firstName;
	private String lastName;
	private String title;

	public UWECPerson(int uwecId, String firstName, String lastName) {
		this.uwecId = uwecId;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public final int getUwecId() {
		return this.uwecId;
	}

	public final String getFirstName() {
		return this.firstName;
	}

	public final String getLastName() {
		return this.lastName;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public final String getTitle() {
		return this.title;
	}

	public abstract String toString();
	// return "UWECPerson = uwecId: " + getUwecId() + ", name: " + getFirstName() +
	// " " + getLastName();

	public boolean equals(Object other) {
		if (other instanceof UWECPerson) {
			UWECPerson p = (UWECPerson) other;
			if (p.firstName.equals(this.firstName) && p.lastName.equals(this.lastName) && p.uwecId == this.uwecId) {
				return true;
			}
		}
		return false;
	}
}
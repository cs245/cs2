
public class UWECFaculty extends UWECAcademic implements UWECEmployee {
	private double yearlySalary;

	public UWECFaculty(int uwecId, String firstName, String lastName, int numTotalCredits) {
		super(uwecId, firstName, lastName);
		setNumTotalCredits(numTotalCredits);
	}

	public void setNumTotalCredits(int numTotalCredits) {
		super.setNumTotalCredits(numTotalCredits);
		if (numTotalCredits < 48) {
			super.setTitle("Adjunct Professor");
		} else if (numTotalCredits < 120) {
			super.setTitle("Assistant Professor");
		} else if (numTotalCredits < 240) {
			super.setTitle("Associate Professor");
		} else
			super.setTitle("Professor");
	}
	public final double getYearlySalary() {
		return yearlySalary;
	}

	public void setYearlySalary(double yearlySalary) {
		this.yearlySalary = yearlySalary;
	}

	public double computePaycheck() {
		return yearlySalary / 26;
	}

	public String toString() {
		return "UWECFaculty = uwecId: "+super.getUwecId()+", name: "+ super.getFirstName()+" "+super.getLastName()+", title: "+super.getTitle()+", credits: "+super.getNumTotalCredits()+", yearlySalary: "+yearlySalary;
	}

	public boolean equals(Object other) {
		if (other instanceof UWECFaculty) {
			UWECFaculty p = (UWECFaculty) other;
			if (super.equals(other) && this.yearlySalary == p.yearlySalary) {
				return true;
			}
		}
		return false;
	}

}

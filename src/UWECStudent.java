
public class UWECStudent extends UWECAcademic {

	private double gpa;

	public UWECStudent(int uwecId, String firstName, String lastName, double gpa) {
		super(uwecId, firstName, lastName);
		this.gpa = gpa;
		setNumTotalCredits(0);
	}
	
	public void setNumTotalCredits(int numTotalCredits) {
		super.setNumTotalCredits(numTotalCredits);
		if (numTotalCredits < 24 ) {
			super.setTitle("Freshman");
		} else if (numTotalCredits < 58) {
			super.setTitle("Sophomore");
		} else if (numTotalCredits < 86) {
			super.setTitle("Junior");
		} else if (numTotalCredits > 85){
			super.setTitle("Senior");
		}
	}

	public final double getGpa() {
		return this.gpa;
	}

	public String toString() {
		return "UWECStudent = uwecId: " + getUwecId() + ", name: " + getFirstName() + " " + getLastName() + ", title: "
				+ super.getTitle() + ", credits: " + super.getNumTotalCredits() + ", gpa: " + this.gpa;
//		"UWECStudent = uwecId: 2, name: first2 last2, title: Senior, credits: 86, gpa: 3.9"
	}

	public boolean equals(Object other) {
		if (other instanceof UWECStudent) {
			UWECStudent p = (UWECStudent) other;
			if (super.equals(other) && this.gpa == p.gpa) {
				return true;
			}
		}
		return false;
	}
}

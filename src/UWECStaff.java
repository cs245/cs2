

public class UWECStaff extends UWECPerson implements UWECEmployee{

	private double hourlyPay;
	private double hoursPerWeek;

	public UWECStaff(int uwecId, String firstName, String lastName, String title) {
		super(uwecId, firstName, lastName);
		super.setTitle(title);
	}

	public final double getHourlyPay() {
		return this.hourlyPay;
	}

	public void setHourlyPay(double hourlyPay) {
		this.hourlyPay = hourlyPay;
	}

	public final double getHoursPerWeek() {
		return this.hoursPerWeek;
	}

	public void setHoursPerWeek(double hoursPerWeek) {
		this.hoursPerWeek = hoursPerWeek;
	}

	public double computePaycheck() {
		return 2 * hoursPerWeek *  hourlyPay;
	}

	public String toString() {
		return "UWECStaff = uwecId: " + getUwecId() + ", name: " + getFirstName() + " " + getLastName() + ", title: "
				+ super.getTitle() + ", hourly pay: " + this.hourlyPay + ", hours/week: " + this.hoursPerWeek;
	}

	public boolean equals(Object other) {
		if (other instanceof UWECStaff) {
			UWECStaff p = (UWECStaff) other;
			if (super.equals(p) && p.hourlyPay == this.hourlyPay && p.hoursPerWeek == this.hoursPerWeek) {
				return true;
			}
		}
		return false;
	}

}

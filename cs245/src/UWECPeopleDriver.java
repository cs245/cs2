
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class UWECPeopleDriver {

	public UWECPeopleDriver() {
		// assignment2Input2.txt
		// assignment2Output2.txt
	}

	public static void main(String[] args) throws FileNotFoundException {
		ArrayList<UWECPerson> universityPeople = new ArrayList<UWECPerson>();
		Scanner in = new Scanner(System.in);

		System.out.print("Enter the name of the input file: ");
		String inputFile = in.nextLine();
		Scanner fileIn = new Scanner(new File(inputFile));

		System.out.print("Enter the name of the output file: ");
		String outputFile = in.nextLine();
		PrintWriter printer = new PrintWriter(new File(outputFile));

		boolean quit = false;

		while (!quit) {
			int option = getMenuChoice(fileIn);
			if (option == 1) {
				// addstudent
				addStudent(fileIn, universityPeople);
			} else if (option == 2) {
				// add staff
				addStaff(fileIn, universityPeople);
			} else if (option == 3) {
				// add faculty
				addFaculty(fileIn, universityPeople);
				// option = getMenuChoice(in);
			} else if (option == 4) {
				// compute total payroll
				computeTotalPayroll(printer, universityPeople);
			} else if (option == 5) {
				// print directory
				printDirectory(printer, universityPeople);
			} else if (option == 6) {
				// quit
				quit = true;
			}
		}
		in.close();
		printer.close();
		fileIn.close();
	}

	public static void addStudent(Scanner fileIn, ArrayList<UWECPerson> universityPeople) {
		try {
			// scan next int and store as uwecId
			int uwecId = fileIn.nextInt();
			// scan next string and store as first name
			String fName = fileIn.next();
			// scan next string and store as last name
			String lName = fileIn.next();
			int credits = fileIn.nextInt();// ?
			// scan next double and stores as gpa
			double gpa = fileIn.nextDouble();

			UWECStudent s = new UWECStudent(uwecId, fName, lName, gpa);
			s.setNumTotalCredits(credits);
			universityPeople.add(s);
		} catch (InputMismatchException e) {
			System.out.println("Unexpected input received in addStudent.");
		} 
		fileIn.nextLine();
	}

	public static void addStaff(Scanner fileIn, ArrayList<UWECPerson> universityPeople) {
		try {
			// scan in next int and store as id
			int id = fileIn.nextInt();
			// scan in next string and store as first name
			String fName = fileIn.next();
			// scan in next string and store as last name
			String lName = fileIn.next();
			// scan in next string and store as title
			String title = fileIn.next();
			// scan in next double and store as hourlyPay
			double hourlyPay = fileIn.nextDouble();
			// scan in next double and store as hoursPerWeek
			double hoursPerWeek = fileIn.nextDouble();

			UWECStaff s = new UWECStaff(id, fName, lName, title);

			s.setHourlyPay(hourlyPay);
			s.setHoursPerWeek(hoursPerWeek);
			s.setTitle(title);

			universityPeople.add(s);
			fileIn.nextLine();
		} catch (InputMismatchException e) {
			System.out.println("Unexpected input received in addStaff.");
//			System.out.println(fileIn.nextLine());
			fileIn.nextLine();
		}

	}

	public static void addFaculty(Scanner fileIn, ArrayList<UWECPerson> universityPeople) {
		try {// scan in next int and store as id
			int id = fileIn.nextInt();
			// scan in next String and store as first name
			String fName = fileIn.next();
			// scan in next string and store as last name
			String lName = fileIn.next();
			// scan in next int, convert it to a title and store it as a title
			int nTotalCredits = fileIn.nextInt();
			// scan in next double and store as yearly salary
			double yearlySalary = fileIn.nextDouble();

			UWECFaculty f = new UWECFaculty(id, fName, lName, nTotalCredits);
			f.setYearlySalary(yearlySalary);

			universityPeople.add(f);
		} catch (InputMismatchException e) {
			System.out.println("Unexpected input received in addFaculty.");
			fileIn.nextLine();
		}
	}

	public static void printDirectory(PrintWriter printer, ArrayList<UWECPerson> universityPeople) {
		for (int i = 0; i < universityPeople.size(); i++) {
			printer.println(universityPeople.get(i).toString());
		}

	}

	public static void computeTotalPayroll(PrintWriter printer, ArrayList<UWECPerson> universityPeople) {
		// compute and print the sum of all UWECEmployee's paychecks
		double sum = 0;
		for (int i = 0; i < universityPeople.size(); i++) {
			// figuring out which persons in universityPeople arrayList are staff
			if (universityPeople.get(i) instanceof UWECEmployee) {
				UWECEmployee e = (UWECEmployee) universityPeople.get(i);
				// get the paycheck # and add to sum
				sum += e.computePaycheck();
			}
		}
		printer.printf("Total Payroll: $%.2f%n", sum);
	}

	public static int getMenuChoice(Scanner fileIn) {
		try {
			int option = fileIn.nextInt();
			fileIn.nextLine();
			return option;
		} catch (InputMismatchException e) {
			System.out.println("Non-integer entered in getMenuChoice.");
			return 6;
		}
	}
}